﻿namespace Laboratorna_1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.info_about_shops = new System.Windows.Forms.TextBox();
            this.get_info_shops = new System.Windows.Forms.Button();
            this.name_shop = new System.Windows.Forms.TextBox();
            this.number_shop = new System.Windows.Forms.TextBox();
            this.addres_shop = new System.Windows.Forms.TextBox();
            this.name_shop_lbl = new System.Windows.Forms.Label();
            this.number_shop_lbl = new System.Windows.Forms.Label();
            this.addres_shop_lbl = new System.Windows.Forms.Label();
            this.write_data = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // info_about_shops
            // 
            this.info_about_shops.Location = new System.Drawing.Point(549, 15);
            this.info_about_shops.Margin = new System.Windows.Forms.Padding(4);
            this.info_about_shops.Multiline = true;
            this.info_about_shops.Name = "info_about_shops";
            this.info_about_shops.ReadOnly = true;
            this.info_about_shops.Size = new System.Drawing.Size(429, 483);
            this.info_about_shops.TabIndex = 0;
            // 
            // get_info_shops
            // 
            this.get_info_shops.Location = new System.Drawing.Point(648, 506);
            this.get_info_shops.Margin = new System.Windows.Forms.Padding(4);
            this.get_info_shops.Name = "get_info_shops";
            this.get_info_shops.Size = new System.Drawing.Size(245, 52);
            this.get_info_shops.TabIndex = 1;
            this.get_info_shops.Text = "Отримати данні";
            this.get_info_shops.UseVisualStyleBackColor = true;
            this.get_info_shops.Click += new System.EventHandler(this.GetInfoShops_Click);
            // 
            // name_shop
            // 
            this.name_shop.Location = new System.Drawing.Point(16, 34);
            this.name_shop.Margin = new System.Windows.Forms.Padding(4);
            this.name_shop.Name = "name_shop";
            this.name_shop.Size = new System.Drawing.Size(124, 22);
            this.name_shop.TabIndex = 2;
            // 
            // number_shop
            // 
            this.number_shop.Location = new System.Drawing.Point(149, 34);
            this.number_shop.Margin = new System.Windows.Forms.Padding(4);
            this.number_shop.Name = "number_shop";
            this.number_shop.Size = new System.Drawing.Size(124, 22);
            this.number_shop.TabIndex = 3;
            // 
            // addres_shop
            // 
            this.addres_shop.Location = new System.Drawing.Point(283, 34);
            this.addres_shop.Margin = new System.Windows.Forms.Padding(4);
            this.addres_shop.Name = "addres_shop";
            this.addres_shop.Size = new System.Drawing.Size(124, 22);
            this.addres_shop.TabIndex = 4;
            // 
            // name_shop_lbl
            // 
            this.name_shop_lbl.Location = new System.Drawing.Point(16, 11);
            this.name_shop_lbl.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.name_shop_lbl.Name = "name_shop_lbl";
            this.name_shop_lbl.Size = new System.Drawing.Size(125, 20);
            this.name_shop_lbl.TabIndex = 5;
            this.name_shop_lbl.Text = "Назва магазину";
            // 
            // number_shop_lbl
            // 
            this.number_shop_lbl.Location = new System.Drawing.Point(149, 11);
            this.number_shop_lbl.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.number_shop_lbl.Name = "number_shop_lbl";
            this.number_shop_lbl.Size = new System.Drawing.Size(125, 20);
            this.number_shop_lbl.TabIndex = 6;
            this.number_shop_lbl.Text = "Номер магазину";
            // 
            // addres_shop_lbl
            // 
            this.addres_shop_lbl.Location = new System.Drawing.Point(283, 11);
            this.addres_shop_lbl.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.addres_shop_lbl.Name = "addres_shop_lbl";
            this.addres_shop_lbl.Size = new System.Drawing.Size(136, 20);
            this.addres_shop_lbl.TabIndex = 7;
            this.addres_shop_lbl.Text = "Адреса магазину";
            // 
            // write_data
            // 
            this.write_data.Location = new System.Drawing.Point(81, 80);
            this.write_data.Margin = new System.Windows.Forms.Padding(4);
            this.write_data.Name = "write_data";
            this.write_data.Size = new System.Drawing.Size(245, 52);
            this.write_data.TabIndex = 8;
            this.write_data.Text = "Записати данні";
            this.write_data.UseVisualStyleBackColor = true;
            this.write_data.Click += new System.EventHandler(this.WriteData_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(996, 572);
            this.Controls.Add(this.write_data);
            this.Controls.Add(this.addres_shop_lbl);
            this.Controls.Add(this.number_shop_lbl);
            this.Controls.Add(this.name_shop_lbl);
            this.Controls.Add(this.addres_shop);
            this.Controls.Add(this.number_shop);
            this.Controls.Add(this.name_shop);
            this.Controls.Add(this.get_info_shops);
            this.Controls.Add(this.info_about_shops);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();
        }

        private System.Windows.Forms.TextBox number_shop;
        private System.Windows.Forms.TextBox addres_shop;
        private System.Windows.Forms.Button write_data;

        private System.Windows.Forms.TextBox name_shop;

        private System.Windows.Forms.Label name_shop_lbl;
        private System.Windows.Forms.Label number_shop_lbl;
        private System.Windows.Forms.Label addres_shop_lbl;

        private System.Windows.Forms.Button get_info_shops;

        private System.Windows.Forms.TextBox info_about_shops;

        #endregion
    }
}