﻿namespace Laboratorna_1.Models
{
    public class ShopViewModel
    {
        public string Name;
        public string Number;
        public string Adress;
    
        public ShopViewModel(string name, string number, string adress)
        {
            this.Name = name;
            this.Number = number;
            this.Adress = adress;
        }
    
        public string ForFile()
        {
            return Name + " " + Number + " " + Adress + "\n";
        }
    }
}