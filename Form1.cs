﻿using System;
using System.IO;
using System.Windows.Forms;
using Laboratorna_1.Models;

namespace Laboratorna_1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void GetInfoShops_Click(object sender, EventArgs e)
        {
            StreamReader reader = new StreamReader("./info.txt");

            if (!reader.EndOfStream)
            {
                foreach (var info in reader.ReadToEnd())
                {
                    this.info_about_shops.Text += info;
                }
            }

            reader.Close();
        }

        private void WriteData_Click(object sender, EventArgs e)
        {
            StreamWriter writer = new StreamWriter("./info.txt");
            String name = this.name_shop.Text, number = this.number_shop.Text, addres = this.addres_shop.Text;
            
            writer.WriteLine(new ShopViewModel(name, number, addres).ForFile());
            
            writer.Close();
        }
    }
}